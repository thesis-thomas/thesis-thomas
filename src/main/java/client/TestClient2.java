package client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.BundleEntry;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;
import ca.uhn.fhir.model.dstu2.composite.QuantityDt;
import ca.uhn.fhir.model.dstu2.resource.Bundle;
import ca.uhn.fhir.model.dstu2.resource.Observation;
import ca.uhn.fhir.model.dstu2.resource.Bundle.Entry;
import ca.uhn.fhir.model.dstu2.resource.Patient;
import ca.uhn.fhir.model.primitive.BooleanDt;
import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.client.IGenericClient;

public class TestClient2 {


	private static FhirContext ctx = FhirContext.forDstu2();
	public static void main(String[] args) {

		
		String test = "<Bundle xmlns=\"http://hl7.org/fhir\"><id value=\"1\"/><type value=\"transaction\"/><entry><resource><Patient xmlns=\"http://hl7.org/fhir\"><identifier><value value=\"bmFuY3lhbm46YWJjZDEyMzQ=\"/></identifier></Patient></resource></entry><entry><resource><Observation xmlns=\"http://hl7.org/fhir\"><extension url=\"isBeforeMeal\"><valueBoolean value=\"true\"/></extension><extension url=\"transferTime\"><valueString value=\"2016-07-23T14:57:35.795Z\"/></extension><status value=\"preliminary\"/><code><coding><system value=\"286.BS#BLOODSUGARMEASUREMENTS\"/><version value=\"0.1\"/><code value=\"40\"/><display value=\"BloodSugarMeasurements\"/></coding><text value=\"Blodsukker (manuel)\"/></code><issued value=\"2016-07-23T14:57:35.795Z\"/><valueQuantity><value value=\"9\"/></valueQuantity><interpretation><coding><system value=\"String\"/><code value=\"GREEN\"/><display value=\"286.BS##SEVERITY\"/></coding></interpretation></Observation></resource></entry></Bundle>";
		ca.uhn.fhir.model.api.Bundle bundle = ctx.newXmlParser().parseBundle(test);
		System.out.println("Parsed: " +  bundle.toString());

		List<BundleEntry> list = bundle.getEntries();
		Patient p = bundle.getResources(Patient.class).get(0);
		Observation o = bundle.getResources(Observation.class).get(0);
		System.out.println("Identifier: " + p.getIdentifierFirstRep());
		System.out.println("Extension: " + o.getValue());
		System.out.println("Test: " + list.get(1));
		
		//Patient
		IdentifierDt id = p.getIdentifierFirstRep();
		String authentication = id.getValue();
		
		//Observation
		String name = o.getCode().getText();
		long QuestionnaireId = Long.valueOf(o.getCode().getCoding().get(0).getCode()).longValue();
		String version = o.getCode().getCoding().get(0).getVersion();
		String outputName = o.getCode().getCoding().get(0).getSystem();
		String outputType = o.getCode().getCoding().get(0).getDisplay();
		QuantityDt qd = (QuantityDt) o.getValue();
		String measurementsResult = qd.getValue().toString();
		BooleanDt bd = (BooleanDt) o.getUndeclaredExtensionsByUrl("isBeforeMeal").get(0).getValue();
		boolean measurementsIsBeforeMeal = bd.getValue(); //if false also isAfterMeal
		String measurementsTimeOfMeasurement = o.getIssued().toString(); //Might need to be adjusted to right format
		String transferTime = o.getUndeclaredExtensionsByUrl("transferTime").get(0).getValue().toString();
		String severityName = o.getInterpretation().getCoding().get(0).getDisplay();
		String severityValue = o.getInterpretation().getCoding().get(0).getCode();
		String severityType = o.getInterpretation().getCoding().get(0).getSystem();
		String date = "No date here yet!";
		
		System.out.println("Last: " + qd.getValue().toString());
		
		/*
		
		JSONObject json = new JSONObject();
		json.put("name", name);
		json.put("QuestionnaireId", QuestionnaireId);
		json.put("version", version);
		JSONArray jsonArray = new JSONArray();
		JSONObject output = new JSONObject();
		output.put("name", outputName);
		output.put("type", outputType);
		JSONArray measurements = new JSONArray();
		
		JSONObject value = new JSONObject();
		JSONObject measurement = new JSONObject();
		measurement.put("result", measurementsResult);
		measurement.put("isBeforeMeal", measurementsIsBeforeMeal);
		if (!measurementsIsBeforeMeal) {
			measurement.put("isAfterMeal", true);
		}
		measurement.put("timeOfMeasurement", measurementsTimeOfMeasurement);
		
		measurements.add(measurement);
		
		value.put("measurements", measurements);
		value.put("transferTime", transferTime);
		
		output.put("value",value);
		
		
		JSONObject severity = new JSONObject();
		severity.put("name", severityName);
		severity.put("value", severityValue);
		severity.put("type", severityType);
		
		jsonArray.add(0, output);
		jsonArray.add(1, severity);
		json.put("output", jsonArray);
		
		json.put("date", date);
		
		
		System.out.println("Json: " + json.toJSONString());
		*/
		//Do the measurements
		HashMap<String, Object> mapJsonObect = new HashMap<String, Object>();
		HashMap<Integer, Object> mapJsonArray = new HashMap<Integer, Object>();
		
		mapJsonObect.put("result", measurementsResult);
		mapJsonObect.put("isBeforeMeanl", measurementsIsBeforeMeal);
		if (!measurementsIsBeforeMeal) {
			mapJsonObect.put("isAfterMeal", true);
		}
		mapJsonObect.put("timeOfMeasurement", measurementsTimeOfMeasurement);
		JSONObject measurement1 = createJsonObjectUsingHashMap(mapJsonObect);
		
		//Create measurements array
		mapJsonArray.put(0, measurement1);
		JSONArray measurements1 = createJsonArrayUsingHashMap(mapJsonArray);
		
		//Do the value
		mapJsonObect = new HashMap<String, Object>();
		mapJsonObect.put("measurements", measurements1);
		mapJsonObect.put("transferTime", transferTime);
		JSONObject value1 = createJsonObjectUsingHashMap(mapJsonObect);
		
		//Do the severity
		mapJsonObect = new HashMap<String, Object>();
		mapJsonObect.put("name", severityName);
		mapJsonObect.put("value", severityValue);
		mapJsonObect.put("type", severityType);
		JSONObject output1 = createJsonObjectUsingHashMap(mapJsonObect);
		
		//Do the output
		mapJsonObect = new HashMap<String, Object>();
		mapJsonObect.put("name", outputName);
		mapJsonObect.put("type", outputType);
		mapJsonObect.put("value", value1);
		JSONObject output0 = createJsonObjectUsingHashMap(mapJsonObect);
		
		//Create output array
		mapJsonArray = new HashMap<Integer, Object>();
		mapJsonArray.put(0,  output0);
		mapJsonArray.put(1,  output1);
		JSONArray combinedOutputArray = createJsonArrayUsingHashMap(mapJsonArray);
		
		//Combine all json
		mapJsonObect = new HashMap<String, Object>();
		mapJsonObect.put("name", name);
		mapJsonObect.put("QuestionnaireId", QuestionnaireId);
		mapJsonObect.put("version", version);
		mapJsonObect.put("date", date);
		mapJsonObect.put("output", combinedOutputArray);
		JSONObject json1 = createJsonObjectUsingHashMap(mapJsonObect);
		
		System.out.println("Trying hashmap: " + json1.toJSONString());
	}
	
	private static JSONObject createJsonObjectUsingHashMap(HashMap<String, Object> map) {
		JSONObject json = new JSONObject();
		for (java.util.Map.Entry<String, Object> entry : map.entrySet()) {
			json.put(entry.getKey(), entry.getValue());
		}
		return json;
	}
	
	private static JSONArray createJsonArrayUsingHashMap(HashMap<Integer, Object> map) {
		JSONArray json = new JSONArray();
		for (java.util.Map.Entry<Integer, Object> entry : map.entrySet()) {
			json.add(entry.getKey(), entry.getValue());
		}
		
		return json;
	}
}


